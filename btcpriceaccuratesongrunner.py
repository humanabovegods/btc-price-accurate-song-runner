#!/usr/bin/python
# -*- coding: utf-8 -*-
import urllib.request
import json
import time
import webbrowser

dontgobellow = 27700
goabove = 27750
thisistheend = 'https://www.youtube.com/watch?v=RuiXUySUw70'
congratulations = 'https://www.youtube.com/watch?v=AteCdXvZOZc'

with urllib.request.urlopen(
        'https://bitbay.net/API/Public/BTCPLN/ticker.json'
                            ) as url:
    data = json.loads(url.read().decode())
course = data['last']

while course < goabove and course >= dontgobellow:
    with urllib.request.urlopen(
        'https://bitbay.net/API/Public/BTCPLN/ticker.json'
                                ) as url:
        data = json.loads(url.read().decode())
        course = data['last']
        print ('Last bibay bitcoin transaction in PLN: ' + str(course))
    time.sleep(30)

if course < dontgobellow:
    print ('price gone bellow: ' + str(dontgobellow))
    print ('bitcoin is worth ' + str(course) + 'PLN')
    webbrowser.open_new(thisistheend)
elif course >= goabove:
    print ('price gone above: ' + str(goabove))
    print ('bitcoin is worth ' + str(course) + 'PLN')
    webbrowser.open_new(congratulations)

time.sleep(120)
